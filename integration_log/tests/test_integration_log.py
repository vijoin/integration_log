from odoo.tests.common import TransactionCase
from odoo import fields


class TestIntegration(TransactionCase):

    def SetUp(self, *args, **kwargs):
        super(TestIntegration, self).SetUp(*args, **kwargs)

        # Demo user will be used to run tests
        demo_user = self.env.ref('base.user_demo')
        self.integration_obj = self.env['integration_log.integration'].sudo(
            demo_user)

        # Create two Integrations Log Records
        self.integration1 = self.integration_obj.create({
            'name': '0001', #No need for name. It's a self assigned sequence
            'user_id': demo_user.id,
            'date': fields.Date.context_today(self),
            'state': 'draft',
            #'line_ids': 0,0,{values},
        })
        self.integration2 = self.integration_obj.create({
            'name': '0002', #No need for name. It's a self assigned sequence
            'user_id': demo_user.id,
            'date': fields.Date.context_today(self),
            'state': 'draft',
            # 'line_ids': 0,0,{values},
        })

    def test_populate_integrations(self):
        """Change states for all pending integrations"""
        #self.integration_obj.update_pending()

        demo_user = self.env.ref('base.user_demo')
        integration_obj = self.env['integration_log.integration'].sudo(
            demo_user)
        integration_obj.update_pending()
        count = integration_obj.search_count([('state','=', 'done')])
        self.assertEqual(
            count, 2,
            'Expected 2 integrations done'
        )

