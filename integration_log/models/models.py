# -*- coding: utf-8 -*-

from odoo import models, fields, api

import logging

_logger = logging.getLogger(__name__)


class Integration(models.Model):
    _name = 'integration_log.integration'

    name = fields.Char() #ToDo: Default sequence
    date = fields.Date()
    user_id = fields.Many2one('res.users', default=lambda self: self.env.user)
    state = fields.Selection([('draft', 'Draft'), ('done', 'Donde')],
                             default='draft')
    #ToDo: line_ids = fields.One2many('integration_log.integration.line',
    #ToDo:                            'integration_id')

    def update_pending(self):

        obj_ids = self.search([('state', '=', 'draft')])
        _logger.info("ids draft: %s" % obj_ids)

        obj_ids.write({'state': 'done'})

        obj_ids = self.search([('state', '=', 'done')])
        _logger.info("ids done: %s" % obj_ids)


'''ToDo:
class IntegrationLine(models.Model):
    _name = 'integration_log.integration.line'
    _rec_name = 'name'
    _description = 'New Description'

    Modelo: integration.lines
    campos:

    module_id: m2o
    hacia
    integration.module
    repository_id: m2o
    hacia
    integration.repository
    server_id: m2o
    hacia
    integration.server
    db_id: m2o
    hacia
    integration.server.bd
    description: Text
    state: selection(testing, confirmed, production)
    validator_ids: m2m
    to
    res.partner(widget
    many2many_tags)
'''